


$(()=>{

	let started = false;
	let lineCompleted = false;
	let stopFollowing = false;

	let width =  0;
	let height = 0;
	let $stickyheader = $("#stickyheader");
	let $svg = $("svg");
	let $lineH = $("#linea1");
	let $lineV = $("#linea2");
	let $text1 = $("#text1");
	let $text2 = $("#text2");
	let $logo = $("#logo");
	let logoW = +$logo.attr("width");
	let logoH = +$logo.attr("height");

	const isATouchDevice = isTouchDevice();
	const logoVelocity = isATouchDevice ? 0.05 : .5;

	const ARROW_MARGIN = 10;
	const HORIZONTAL_MARGIN = ARROW_MARGIN * 5;
	const VERTICAL_MARGIN = ARROW_MARGIN * 5;

	window.onresize = resize.bind(this);	
	// if(!isATouchDevice) window.onscroll = onScroll.bind(this);
	resize();

	let mx;
	let my;
	let cx;
	let cy;

	if(!isATouchDevice) {	
		cx = mx = 0;
		cy = my =  0;
		$svg.mousemove(onMove.bind(this));
	} else {
		mx = cx = width *.5; 
		my = cy = height *.5;
		// $svg.on("touchstart", onTouchMove.bind(this));
		$logo.attr("transform", `translate(${mx-logoW*.5},${my-logoH*.5})`);
		introAnimation();
	}

	function resize() {
		width =  window.document.body.clientWidth;
		height = window.document.body.clientHeight;
		$svg.attr("width",width).attr("height", height);
		$("#godown").attr("transform", `translate(${width*.5 - 10},${height - VERTICAL_MARGIN *.7})`);
	}



	function onScroll() {

		if(!started) {
			start();
			window.onscroll = null;
		} 
		
	}

	function onMove(e) {
		mx = e.clientX;
		my = e.clientY;
		clampPosition();
		if(!started) start();
	}

	function onTouchMove(e) {
		mx = e.originalEvent.touches[0].clientX;
		my = e.originalEvent.touches[0].clientY;
		clampPosition();
		render();
	}

	
	function clampPosition() {
		mx = Math.max(mx, logoW*.5 + HORIZONTAL_MARGIN);
		mx = Math.min(mx, width - logoW*.5 - HORIZONTAL_MARGIN);
		my = Math.max(my, logoH*.5 +  VERTICAL_MARGIN);
		my = Math.min(my, height - logoH*.5  - 100);
	}


	function start() {
		started = true;
		render();
		introAnimation();
	}

	function introAnimation() {
		let o = {
			a: 0
		};

		TweenMax.to($("#godown")[0], .3, {delay:1, opacity:1});
		TweenMax.to($logo[0], .3, {delay:.3, opacity:1});

		TweenMax.to(o, .3, {a:1, onUpdate:()=>{
			updateArrows((mx - logoW *.5) * o.a, (my-logoH * .5) * o.a);
		}, onComplete:()=>{
			lineCompleted = true;
		}});
	}



	function updateArrows(lx, ly) {

		let x1;
		let x2;
		if(lx > width *.5) {
			x2 = width - ARROW_MARGIN;
			x1 = lx + logoW + ARROW_MARGIN;
		} else {
			x1 = ARROW_MARGIN;
			x2 = lx-ARROW_MARGIN;
		}

		//update text
		$text1.attr("transform", `translate(${mx+8},${(ly)*.5})`).text(Math.round(my)+"px");
		$text2.attr("transform", `translate(${(x2- x1)*.5 + x1},${my-8})`).text(Math.round(mx)+"px");
		// update arrows
		moveArrow($lineH, x1, my, x2, my);
		moveArrow($lineV, mx, ARROW_MARGIN, mx, ly - ARROW_MARGIN);
	}

	function moveArrow($l, x1,y1,x2,y2){
		$l
		.attr("x1", x1)
		.attr("x2", x2)
		.attr("y1", y1)
		.attr("y2", y2);
	}

	function render() {

		if(!stopFollowing) {
			cx += (mx-logoW*.5 - cx)*logoVelocity;
			cy += (my-logoH*.5 - cy) *logoVelocity;
			// move logo under mouse
			$logo.attr("transform", `translate(${cx},${cy})`);
			if(lineCompleted) updateArrows(cx, cy);
		}
		window.requestAnimationFrame(render);

		if(isATouchDevice) return;

		if(window.scrollY > height - $stickyheader.height()) {

			let n = 1 -(height - scrollY)/ $stickyheader.height();
			if(n <= 1) {
				$stickyheader.css("transform", `translate(0, ${-100*(1-n)}%)`);
			} else {
				$stickyheader.css("transform", `translate(0, 0)`);
			}
			
		} else {
			$stickyheader.css("transform", `translate(0, -130%)`);
		}

	}

	// https://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript
	function isTouchDevice() {
		return 'ontouchstart' in window        // works on most browsers 
		|| navigator.maxTouchPoints;       // works on IE10/11 and Surface
	}



});
