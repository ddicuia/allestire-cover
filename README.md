# README #

Intro / cover per sito Allestire 


### Set up ###

I file compilati e ottimizzati sono nella cartella "dist". Se non si necessita di modificare il javascript si può anche saltare la fase di sviluppo (vedi in basso).

### Sviluppo ###

È necessiario installare tutte le dipendenze. Per farlo bisogna installare (se non lo si ha già) NodeJS.

Una volta fatto eseguire nella cartella del progetto

`npm install` 

`bower install`

Questo installerà le dipendenze necessarie sia per lo sviluppo che per i componenti usati dalla pagina.

### Uso ###

`gulp serve` per far partire un server per mostrare la pagina

`gulp build` per compilare la versione finale che si troverà nella cartella "dist"

